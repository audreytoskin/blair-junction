/* ========================================================
 	First, set up all multimedia assets.
======================================================== */

// The game will render to this canvas element.
var canvas = document.getElementById( "canvas" );

// Create a new CanvasRenderingContext2D object, a collection of properties and methods for drawing on the canvas.
var paintKit = canvas.getContext( "2d" );

// Display size/resolution. Defaults to "standard definition", but gets scaled later.
var display = "sd";

// Keep track of the previous display size, in case user keeps resizing the window.
var displayPrevious;

// The background sand. Actual image file to be determined by display size. (See `resetGraphics()` below.)
var background = new Image();

// Graphics size and position scaling factor, based on display size, determined in `resetGraphics()`.
var scaleGraphics;

// Global default speed, in pixels-per-second, for all animated sprites. Scales for different display sizes.
var SPEED = 150;

// Global default font size for onscreen text, in pixels. Scales for different display sizes.
var FONT_SIZE = 20;

// Global default line-height for onscreen text (a proportion of the font size).
var LINE_HEIGHT = 1.3;

// Global default font family for titles and available action prompts.
var TITLE_FONT = "Cinzel Regular, serif";

// Global default font family for pretty much any other text that appears onscreen.
var PROSE_FONT = "Cardo Regular, serif";

// Adjust graphic settings based on current display size.
var resetGraphics = function () {
	// First, get display size and resize canvas to fit. `canvas` should take up the whole viewport.
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;

	// When adjusting font sizes, it can't scale the same way the graphics will, because it easily becomes illegible, or giant.

	// Adjust graphics setting for small or low-resolution ("sd") displays.
	if ( canvas.width < 1800 && canvas.height < 1800 ) {
		displayPrevious = display;
		display = "sd";
		FONT_SIZE = 20;
	}
	// Adjust graphics setting for very large or ultra-high-resolution ("uhd") displays.
	else if ( canvas.width >= 3600 || canvas.height >= 3600 ) {
		displayPrevious = display;
		display = "uhd";
		FONT_SIZE = 30;
	}
	// Adjust graphics setting for medium-large or high-resolution ("hd") displays.
	else if ( canvas.width >= 1800 || canvas.height >= 1800 ) {
		displayPrevious = display;
		display = "hd";
		FONT_SIZE = 25;
	}

	// Choose background image file based on display size.
	background = document.getElementById( "sand-" + display );

	// Scaling from sd to hd.
	if ( displayPrevious === "sd" && display === "hd" ) {
		scaleGraphics = 2;
	}
	// Scaling from sd to uhd.
	else if ( displayPrevious === "sd" && display === "uhd" ) {
		scaleGraphics = 4;
	}
	// Scaling from hd to sd.
	else if ( displayPrevious === "hd" && display === "sd" ) {
		scaleGraphics = 0.5;
	}
	// Scaling from hd to uhd.
	else if ( displayPrevious === "hd" && display === "uhd" ) {
		scaleGraphics = 2;
	}
	// Scaling from uhd to sd.
	else if ( displayPrevious === "uhd" && display === "sd" ) {
		scaleGraphics = 0.25;
	}
	// Scaling from uhd to hd.
	else if ( displayPrevious === "uhd" && display === "hd" ) {
		scaleGraphics = 0.5;
	}
	// Scaling for only small changes to window size.
	else {
		scaleGraphics = 1;
	}

	// Adjust default speed based on display size.
	SPEED *= scaleGraphics;

	// Scale `sheriff` sprite on canvas (only after it's created).
	if ( sheriff ) {
		sheriff.img = document.getElementById( sheriff.name + "-" + display );

		sheriff.renderX *= scaleGraphics;
		sheriff.renderY *= scaleGraphics;
		sheriff.centerX *= scaleGraphics;
		sheriff.centerY *= scaleGraphics;

		sheriff.topEdge = sheriff.renderY;
		sheriff.bottomEdge = sheriff.renderY + sheriff.img.height;
		sheriff.leftEdge = sheriff.renderX;
		sheriff.rightEdge = sheriff.renderX + sheriff.img.width;

		sheriff.speed = SPEED;
	}
};

// Set graphical settings now.
resetGraphics();

// Reset graphical settings again if the window resizes.
window.addEventListener( "resize", resetGraphics, false );

// Class for creating sprites.
var Sprite = function ( name, x, y, anchorX, anchorY, speedMult, actions ) {
	// Short name for sprite. `name` combined with resolution (e.g., "sheriff-hd") becomes the HTML image element ID.
	this.name = name;

	// An object containing the sprite's actual image file and related properties.
	this.img = document.getElementById( name + "-" + display );

	/*
	 * The `x` and `y` parameters are the x- and y-coordinates for the sprite on the canvas.
	 * But in the Canvas 2D API, the (x,y) coordinates are always the top-left corner of the image; the anchor X and Y parameters
	 * allow you to choose what part of the image corresponds to these initial points. You choose the "anchor" point on the image.
	 * Values should be a float from 0 to 1, inclusive:
	 * An `anchorX` value of 0 places `x` on the left side of the image, 1 puts `x` on the right, and 0.5 in the middle.
	 * And an `anchorY` value of 0 places `y` at the top of the image, 1 puts `y` on the bottom, and 0.5 is the middle.
	 */

	// Rendering coordinates for the sprite. Default to coordinates (0,0) if none are provided.
	this.renderX = x - this.img.width * anchorX || 0;
	this.renderY = y - this.img.height * anchorY || 0;

	// The coordinates for the *center* of the sprite, sometimes more useful for comparing position of sprites relative to each other.
	this.centerX = this.renderX + this.img.width / 2;
	this.centerY = this.renderY + this.img.height / 2;

	// Outer edges keep player from walking through other sprites.
	this.topEdge = this.renderY;
	this.bottomEdge = this.renderY + this.img.height;
	this.leftEdge = this.renderX;
	this.rightEdge = this.renderX + this.img.width;

	/*
	 * The sprite's speed, as a proportion of the global default speed. `speedMult` should be a float close to 1.
	 * Default to global default speed if no multiplier is provided.
	 */
	this.speed = SPEED * speedMult || SPEED;

	// `actions` is an array listing actions the player can do when close enough to interact with another sprite.
	this.actions = actions;

	// Keeps track of the player's current position in the `actions` array.
	this.progress = 0;

	// Keeps track of the player's current position in the dialog arrays when talking.
	this.dialog = 0;

	// Whether the player is currently talking with the NPC.
	this.talking = false;
};

// `sheriff` is the player-controlled character.
var sheriff = new Sprite(
	"sheriff",
	canvas.width / 4,
	0,
	0.5,
	0,
	1
);

// Sheriff isn't moving until player clicks somewhere.
sheriff.movingToPointer = false;

// `cadaver` is the dead body(!!), the first non-player character ("NPC") you see in this murder mystery.
var cadaver = new Sprite(
	"cadaver",
	canvas.width * 3/4,
	canvas.height * 3/4,
	0.5,
	0.5,
	0,
	[
		{
			label: "talk",
			action: [
				/*
				 * Symbols that indicate you can scroll or finish the dialog:
				 * ▲ U+25b2
				 * ▼ U+25bc
				 * ▣ U+25a3
				 */
				"Part 6: …And so, all of the villagers chased Albi the racist dragon into a very cold and very scary cave. And it was so cold, and so scary in there, that Albi began to cry dragon tears. Which as we all know turn into jellybeans!  ▼",
				"▲  Anyway, at that moment he felt a tiny little hand rest upon his tail, and he turned around, and who should that little hand belong to but the badly burnt Albanian boy from the day before.  ▼",
				"▲  Albi: What are you doing here, I thought I killed you yesterday! (grumbled Albi quite racistly)  ▼",
				"▲  Boy: No Albi, you didn’t kill me with your dragon flames. I crawled to safety! But you did leave me very badly disfigured. (laughed the boy.) Why are you crying so?  ▼",
				"▲  Albi: I’m crying because all of those horrible villagers chased me into this scary cave! I think it’s because I’m so racist. Get your hand off my tail, you’ll make it dirty.  ▼",
				"▲  Boy: No Albi, it’s not because of your racism that they chased you here. They chased me here too and I became all disfigured like this. They just don’t like you and I… because… well because we’re different to them.  ▼",
				"▲  And that made Albi cry a single tear, which turned into a jellybean all colors of the rainbow! And suddenly, he wasn’t racist anymore.  ▣"
			]
		}
	]
);


/* ========================================================
	Player input.
======================================================== */

// `pointer` object keeps track of most recent click or tap.
var pointer = {
	x: 0,
	y: 0
};

// Get the pointer's position when you click (or tap) on the canvas.
canvas.addEventListener( "click", function ( event ) {
	pointer.x = event.clientX;
	pointer.y = event.clientY;

	// Whether the player recently clicked/tapped to move somewhere.
	sheriff.movingToPointer = true;
}, false );

// `keyboard` object keeps track of keyboard input.
var keyboard = {};

/*
 * An event listener which detects currently pressed keys and sets them to true in `keyboard`,
 * regardless of `key` or `keyCode` values used by the browser.
 */
window.addEventListener( "keydown", function ( event ) {
	// Direction arrow keys.
	if ( event.key === "ArrowUp" || event.key === "Up" || event.keyCode === 38 ) {
		keyboard.UP = true;
	}
	if ( event.key === "ArrowDown" || event.key === "Down" || event.keyCode === 40 ) {
		keyboard.DOWN = true;
	}
	if ( event.key === "ArrowLeft" || event.key === "Left" || event.keyCode === 37 ) {
		keyboard.LEFT = true;
	}
	if ( event.key === "ArrowRight" || event.key === "Right" || event.keyCode === 39 ) {
		keyboard.RIGHT = true;
	}

	// Left-side home row keys.
	if ( event.key === "a" || event.keyCode === 65 ) {
		keyboard.A = true;
	}
	if ( event.key === "s" || event.keyCode === 83 ) {
		keyboard.S = true;
	}
	if ( event.key === "d" || event.keyCode === 68 ) {
		keyboard.D = true;
	}
	if ( event.key === "f" || event.keyCode === 70 ) {
		keyboard.F = true;
	}

	// Other keys.
	if ( event.key === "Enter" || event.keyCode === 13 ) {
		keyboard.ENTER = true;
	}
	if ( event.key === " " || event.key === "Spacebar" || event.keyCode === 32 ) {
		keyboard.SPACE = true;
	}
	if ( event.key === "Escape" || event.key === "Esc" || event.keyCode === 27 ) {
		keyboard.ESC = true;
	}

	// Moving with the keyboard interrupts moving with mouse or touch.
	sheriff.movingToPointer = false;
}, false );

// An event listener which detects released keys and sets them to false in `keyboard`.
window.addEventListener( "keyup", function ( event ) {
	// Direction arrow keys.
	if ( event.key === "ArrowUp" || event.key === "Up" || event.keyCode === 38 ) {
		keyboard.UP = false;
	}
	if ( event.key === "ArrowDown" || event.key === "Down" || event.keyCode === 40 ) {
		keyboard.DOWN = false;
	}
	if ( event.key === "ArrowLeft" || event.key === "Left" || event.keyCode === 37 ) {
		keyboard.LEFT = false;
	}
	if ( event.key === "ArrowRight" || event.key === "Right" || event.keyCode === 39 ) {
		keyboard.RIGHT = false;
	}

	// Left-side home row keys.
	if ( event.key === "a" || event.keyCode === 65 ) {
		keyboard.A = false;
	}
	if ( event.key === "s" || event.keyCode === 83 ) {
		keyboard.S = false;
	}
	if ( event.key === "d" || event.keyCode === 68 ) {
		keyboard.D = false;
	}
	if ( event.key === "f" || event.keyCode === 70 ) {
		keyboard.F = false;
	}

	// Other keys.
	if ( event.key === "Enter" || event.keyCode === 13 ) {
		keyboard.ENTER = false;
	}
	if ( event.key === " " || event.key === "Spacebar" || event.keyCode === 32 ) {
		keyboard.SPACE = false;
	}
	if ( event.key === "Escape" || event.key === "Esc" || event.keyCode === 27 ) {
		keyboard.ESC = false;
	}
}, false );

// Move the player through the world.
var move = function ( direction ) {
	/*
	 * Distance equals speed multiplied by time, see? Elementary.
	 * Also means player moves onscreen at the same speed, even if the game runs slowly for some reason.
	 */
	var distance = sheriff.speed * elapsedTime;

	if ( direction === "north" ) {
		// Note that "up" is negative, because the (x,y) origin (0,0) is in the top-left, not the center.
		sheriff.renderY -= distance;
		sheriff.centerY -= distance;
		sheriff.topEdge -= distance;
		sheriff.bottomEdge -= distance;
	}
	else if ( direction === "south" ) {
		sheriff.renderY += distance;
		sheriff.centerY += distance;
		sheriff.topEdge += distance;
		sheriff.bottomEdge += distance;
	}
	else if ( direction === "west" ) {
		sheriff.renderX -= distance;
		sheriff.centerX -= distance;
		sheriff.leftEdge -= distance;
		sheriff.rightEdge -= distance;
	}
	else if ( direction === "east" ) {
		sheriff.renderX += distance;
		sheriff.centerX += distance;
		sheriff.leftEdge += distance;
		sheriff.rightEdge += distance;
	}
};

// Show the currently available actions.
var showActions = function ( sprite ) {
	paintKit.font = String( FONT_SIZE * 1.2 ) + "px " + TITLE_FONT;

	// Draw the box to contain the list of action choices.
	var label = sprite.actions[ sprite.progress ].label;
	var padding = FONT_SIZE / 4;
	var boxWidth = paintKit.measureText( label ).width + padding * 2;
	var boxHeight = FONT_SIZE * 2;
	var boxX = canvas.width / 2 - boxWidth / 2 + padding;
	var boxY = 0;
	paintKit.fillStyle = "hsla(0, 0%, 25%, 0.6)";
	paintKit.fillRect( boxX, boxY, boxWidth, boxHeight );

	// Draw the current available action.
	paintKit.fillStyle = "white";
	paintKit.fillText(
		label,
		boxX + FONT_SIZE/4,
		boxY + FONT_SIZE * LINE_HEIGHT
	);
};

// Show the dialog text. This assumes the current action actually is dialog (and gets tested to make sure, in `render()`).
var showDialog = function ( sprite ) {
	// Reset the font, and the text we're drawing onscreen.
	paintKit.font = FONT_SIZE + "px " + PROSE_FONT;
	var text = sprite.actions[ sprite.progress ].action[ sprite.dialog ];
	// Here, `padding` should work similarly to the CSS padding property.
	var padding = FONT_SIZE / 2;

	/*
	 * Prepare to draw the dialog box.
	 * The box's width will be 20em, plus the padding.
	 */
	var boxWidth = FONT_SIZE * 20 + padding * 2;
	// The box's height depends on the number of wrapped lines, so next we have to process the dialog string.
	var boxHeight = 0;
	var boxX = ( canvas.width - boxWidth ) / 2 + padding;
	var boxY = 0;

	// Split the dialog string into words and lines, so we'll have a neatly wrapped text block when drawing on canvas.
	var block = [];
	var line = "";
	var lineX = boxX + padding;
	var lineY = boxY + padding + FONT_SIZE;
	var words = text.split( " " );

	for( var word in words ) {
		// Test to see if we can add more words to the current line.
		var testLine = line + words[ word ] + " ";
		var testWidth = paintKit.measureText( testLine ).width + padding * 2;
		// If one more word makes the line too long...
		if ( testWidth > boxWidth ) {
			// ...then add the current line to the text block now.
			block.push( line );
			// And start building the next line.
			line = words[ word ] + " ";
		}
		// If the line won't exceded the box width, add the next word.
		else {
			line = testLine;
		}
	}
	// Now add the last line to the block.
	block.push( line );

	// The dialog box's height is based on the number of lines of wrapped text.
	boxHeight = FONT_SIZE * LINE_HEIGHT * block.length + padding * 2;

	// Draw the box to contain the dialog text.
	paintKit.fillStyle = "hsla(0, 0%, 25%, 0.6)";
	paintKit.fillRect( boxX, boxY, boxWidth, boxHeight );

	// Draw the dialog block.
	paintKit.fillStyle = "white";
	for ( var row in block ) {
		paintKit.fillText( block[ row ], lineX, lineY );
		lineY += FONT_SIZE * LINE_HEIGHT;
	}
};


/* ========================================================
	Comparing positions of different sprites.
	We're about to descend into Boolean hell. I'm sorry.
======================================================== */

/*
 * Test if `sheriff` is close enough to a target point (if it's within the radius).
 * This test covers a *circular* area around the target. Return boolean.
 */
var closeEnough = function ( target, radius ) {
	// The distance between two points is the hypotenuse of a right triangle.

	// If target is a sprite with "center" coordinates, test with those.
	if ( target.centerX ) {
		var a = sheriff.centerX - target.centerX;
		var b = sheriff.centerY - target.centerY;
	}
	// ...Otherwise treat target as a point (e.g., the pointer/mouse).
	else {
		var a = sheriff.centerX - target.x;
		var b = sheriff.centerY - target.y;
	}

	var hypotenuse = Math.sqrt( a*a + b*b );

	// Is the hypotenuse within range of our required radius?
	return hypotenuse <= radius;
};

// The next few tests cover *rectangular* areas around the target.

// Test if `sheriff` top edge collides with a target sprite above. Return boolean.
var collidingTop = function ( target ) {
	// Sheriff top hits between target top and bottom.
	return sheriff.topEdge <= target.bottomEdge && sheriff.topEdge >= target.topEdge &&
		// Rule out horizontal misses.
		// (The +/-15 sorta trims the corners of the hit area, so you don't get false positives in the other collision tests.)
		!( sheriff.rightEdge < target.leftEdge + 15 ) &&
		!( sheriff.leftEdge > target.rightEdge -15 );
};

// Test if `sheriff` bottom edge collides with a target below. Return boolean.
var collidingBottom = function ( target ) {
	// Sheriff bottom hits between the target top and bottom.
	return sheriff.bottomEdge >= target.topEdge && sheriff.bottomEdge <= target.bottomEdge &&
		// Rule out horizontal misses.
		!( sheriff.rightEdge < target.leftEdge + 15 ) &&
		!( sheriff.leftEdge > target.rightEdge -15 );
};

// Test if `sheriff` left edge collides with a target. Return boolean.
var collidingLeft = function ( target ) {
	// Sheriff left hits between the target left and right.
	return sheriff.leftEdge <= target.rightEdge && sheriff.leftEdge >= target.leftEdge &&
		// Rule out vertical misses.
		!( sheriff.topEdge > target.bottomEdge - 15 ) &&
		!( sheriff.bottomEdge < target.topEdge + 15 );
};

// Test if `sheriff` right edge collides with a target. Return boolean.
var collidingRight = function ( target ) {
	// Sheriff right hits between the target left and right.
	return sheriff.rightEdge >= target.leftEdge && sheriff.rightEdge <= target.rightEdge &&
		// Rule out vertical misses.
		!( sheriff.topEdge > target.bottomEdge - 15 ) &&
		!( sheriff.bottomEdge < target.topEdge + 15 );
};

// Test if there was *any* collision between `sheriff` and another sprite. Return boolean.
var colliding = function ( target ) {
	return sheriff.leftEdge <= target.rightEdge &&
		sheriff.rightEdge >= target.leftEdge &&
		sheriff.topEdge <= target.bottomEdge &&
		sheriff.bottomEdge >= target.topEdge;
};


/* ========================================================
	Functions that run continuously during play:
	Update the state of the game, and render to the canvas.
======================================================== */

/*
 * For movement, continuously applying the action associated with the arrow keys is good,
 * but when scrolling through dialog, it moves too quickly, so there has to be a small delay,
 * which gets reset with each dialog action.
 */
var delayCounter = 0;
// A global default wait time, in fractions of a second, for any other functions which require a delay.
var WAIT = 0.28;

// Update game objects with every gameLoop.
var update = function () {
	// Wait for the dialog delay first before doing dialog actions.
	if ( delayCounter < WAIT ) {
		delayCounter += elapsedTime;
	}
	// If close enough to an NPC you haven't yet started talking to, and you press Space...
	else if (
		closeEnough( cadaver, sheriff.img.height ) &&
		keyboard.SPACE &&
		!cadaver.talking
	) {
		// ...choosing an action labeled "talk", then you start talking.
		if ( cadaver.actions[ cadaver.progress ].label === "talk" ) {
			cadaver.talking = true;
		}
		delayCounter = 0;
	}
	// If you're talking, and haven't gotten to the end of the dialog, pressing Down or Space moves on to the next dialog block.
	else if (
		cadaver.talking &&
		( keyboard.DOWN || keyboard.SPACE ) &&
		cadaver.dialog < cadaver.actions[ cadaver.progress ].action.length - 1
	) {
		cadaver.dialog++;
		delayCounter = 0;
	}
	// If you're already talking, and you aren't still at the beginning, pressing Up moves back to the previous dialog block.
	else if (
		cadaver.talking &&
		keyboard.UP &&
		cadaver.dialog > 0
	) {
		cadaver.dialog--;
		delayCounter = 0;
	}
	// When you're on the last dialog block, pressing Space again will end the dialog. Reset the sprite's `dialog` index, and increment `progress`.
	else if (
		cadaver.talking &&
		keyboard.SPACE &&
		cadaver.dialog === cadaver.actions[ cadaver.progress ].action.length - 1
	) {
		cadaver.dialog = 0;
		if ( cadaver.progress < cadaver.actions.length - 1 ) {
			cadaver.progress++;
		}
		cadaver.talking = false;
		delayCounter = 0;
	}
	// Pushing Escape while talking will cancel the dialog. Reset the sprite's `dialog` index (but not the `progress`).
	else if ( cadaver.talking && keyboard.ESC ) {
		cadaver.talking = false;
		cadaver.dialog = 0;
		delayCounter = 0;
	}

	// If you aren't talking to anyone, you can move around and such as normal.
	if ( ! cadaver.talking ) {
		// If the player clicks somewhere on the canvas, move to that position.
		if ( sheriff.movingToPointer ) {
			// If the sheriff sprite is close enough, though, *stop* moving.
			if ( closeEnough( pointer, 7 )) {
				sheriff.movingToPointer = false;
			}
			// ...Otherwise, if there are no collisions, continue moving.
			else {
				if ( sheriff.centerX <= pointer.x && !collidingRight( cadaver )) {
					move( "east" );
				}
				if ( sheriff.centerX >= pointer.x && !collidingLeft( cadaver )) {
					move( "west" );
				}
				if ( sheriff.centerY <= pointer.y && !collidingBottom( cadaver )) {
					move( "south" );
				}
				if ( sheriff.centerY >= pointer.y && !collidingTop( cadaver )) {
					move( "north" );
				}
			}
		}

		// If the player is holding the Up arrow key, and if there's no collision on top, move up / "north".
		if ( keyboard.UP && !collidingTop( cadaver )) {
			move( "north" );
		}
		// If the player is holding the Down arrow key, and if there's no collision on bottom, move down / "south".
		if ( keyboard.DOWN && !collidingBottom( cadaver )) {
			move( "south" );
		}
		// If the player is holding the Left arrow key, and if there's no collision on left, move left / "west".
		if ( keyboard.LEFT && !collidingLeft( cadaver )) {
			move( "west" );
		}
		// If the player is holding the Right arrow key, and if there's no collision on right, move right / "east".
		if ( keyboard.RIGHT && !collidingRight( cadaver )) {
			move( "east" );
		}
	}
};


// Current mouse position for debugger.
var debugMouseX, debugMouseY;

canvas.addEventListener( "mousemove", function ( event ) {
	debugMouseX = event.clientX;
	debugMouseY = event.clientY;
}, false );

// Show the current state of the game for debugging.
var debug = function () {
	var debugCanvas = "CANVAS -- width: " + canvas.width +
		" - height: " + canvas.height;
	var debugKeyboard = "KEYBOARD -- ";
	for ( var key in keyboard ) {
		if ( keyboard[ key ]) {
			debugKeyboard += String( key ) + " ";
		}
	}

	var debugMouse = "MOUSE -- coordinates: (" + debugMouseX + ", " + debugMouseY + ")";
	var debugClick = "CLICK -- coordinates: (" + pointer.x + ", " + pointer.y + ")";
	var debugSheriff = "SHERIFF -- width: " + sheriff.img.width +
		" - height: " + sheriff.img.height +
		" - moving to pointer: " + sheriff.movingToPointer;

	paintKit.font = "13px sans-serif";
	var debuggingText = paintKit.measureText( debugSheriff );

	// Draw box to contain the debug info.
	paintKit.fillStyle = "hsla(0, 0%, 25%, 0.5)";
	paintKit.fillRect( 0, 0, debuggingText.width + 20, 120);

	// Draw the debugging text.
	paintKit.fillStyle = "white";
	paintKit.fillText( debugCanvas, 10, 20 );
	paintKit.fillText( debugKeyboard, 10, 40 );
	paintKit.fillText( debugMouse, 10, 60 );
	paintKit.fillText( debugClick, 10, 80 );
	paintKit.fillText( debugSheriff, 10, 100 );
};


// Draw everything on the canvas.
var render = function () {
	// Tile the background image on the canvas.
	for ( var x = 0; x < canvas.width; x += background.width ) {
		for ( var y = 0; y < canvas.height; y += background.height ) {
			paintKit.drawImage( background, x, y );
		}
	}

	// Draw the character sprites.
	paintKit.drawImage( sheriff.img, sheriff.renderX, sheriff.renderY );
	paintKit.drawImage( cadaver.img, cadaver.renderX, cadaver.renderY );

	// If you're talking to an NPC, then show the dialog.
	if ( cadaver.talking ) {
		showDialog( cadaver );
	}
	// Otherwise, if close enough to a sprite, show the available actions.
	else if ( closeEnough( cadaver, sheriff.img.height )) {
		showActions( cadaver );
	}


	// Show the current debug information. Comment out this line to hide.
	debug();
};


/* ========================================================
	Putting all the pieces together...
	Everything above is gathered into one function, `gameLoop()`,
	which loops forever, as long as the game is running.
======================================================== */

/*
 * The UNIX time of the previous loop.
 * If the game were running at 60fps, that ought to be 1/60 of a second ago.
 */
var lastLoop;

// the *length* of time that has passed since the last loop, *in seconds* (should be a fraction, converted from milliseconds).
var elapsedTime;

// `gameLoop` runs recursively forever, as long as the game is running.
var gameLoop = function () {
	// Add gameLoop to the browser's animation loop schedule.
	requestAnimationFrame( gameLoop );

	// Time since navigationStart, more accurate than `Date.now()`.
	var now = performance.now();

	elapsedTime = ( now - lastLoop ) / 1000;

	update();
	render();

	lastLoop = now;

};


/* ========================================================
	Start the game after the whole page finishes loading.
======================================================== */

window.onload = function () {
	lastLoop = performance.now();
	gameLoop();
};
